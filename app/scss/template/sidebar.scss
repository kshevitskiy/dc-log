//## Sidebar nav
@import "_variables";
@import "_mixins";

.menu-wrapper {
    position: fixed;
    right: 0;
    top: 0;
    width: 100%;
    height: 100%;
    max-width: 320px;
    padding: $padding 0;
    background-color: $default;
    z-index: 10;
    box-shadow: 0 0 25px rgba($gray-base, .1);
    transform: translateX(100%);    
    @include transition(transform .5s ease);

    &::after {
        content: '';
        position: absolute;
        width: 100%;
        height: 100%;
        top: 0;
        right: 0;
        box-shadow: 0 0 0 100vw transparent;
        z-index: -1;
        @include transition(box-shadow .3s ease);
    }

    &.active {
        transform: translateX(0);

        &::after {
            box-shadow: 0 0 0 100vw rgba($gray-base, .5);
        }
    }
}

.menu-wrapper {

    .logo-wrapper {
        width: 60px;
        height: 60px;

        .logo {
            height: 55px;            
        }
    }

    .nav-item {
        font-size: $font-size-base;
        color: $gray;
        padding: ($padding / 2) $padding;
        margin-bottom: ($padding / 2);

        &:hover {
            background-color: $gray-lightest;
        }
    }
}

.menu-header {
    padding: 0 $padding;
    margin-bottom: $padding;
    @include flexbox;
    @include align-items(center);
    @include justify-content(space-between);
}