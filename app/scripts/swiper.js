import anime from 'animejs'
import anim from './animations'
import util from './utilities'

const tablet = 922;
const mobile = 768;
const body = document.body;

const pageSlider = [document.querySelector('#page-slider')];
const copySliders = [...document.querySelectorAll('.copy-slider')];
const carousel = [...document.querySelectorAll('.carousel')];

const SWIPER = {

	page (selector) {
		selector.forEach(function(el, index) {
			let hasNav = el.hasAttribute('data-nav');
			let nav = '<div id="page-nav" class="page-nav-wrapper nav-grid"></div>';
			let back = document.createElement('div');
				back.setAttribute('id', 'back-bar');
				back.innerHTML = '<button type="button" class="close"><span>Close page</span></button>';
			let	backTitle = document.createElement('span');
			back.appendChild(backTitle);
				
			hasNav ? el.insertAdjacentHTML('afterend', nav) : '';
			hasNav ? el.parentNode.appendChild(back) : '';
			let navEl = document.getElementById('page-nav');

	        let swiper = new Swiper(el, {
				touchRatio: 0,
				effect: 'fade',
				fadeEffect: {
					crossFade: true
				},
	            grabCursor: false,
	            slidesPerView: 1,
				centeredMode: false,
				speed: 1000,
				preventClicks: false,
				preventClicksPropagation: false,				
				pagination: {
					el: '#page-nav',
					clickable: true,
					bulletClass: 'nav-item',
					bulletActiveClass: 'active',
					renderBullet (index, className) {
						let slide = this.slides[index];
						let iconURL = slide.getAttribute('data-icon');
						let title = slide.getAttribute('title');
						let markup = 
									`
									<div role="button" class="${ className }">
										<i class="icon icon--fluid nav-item__icon" style="background-image: url('${ iconURL }')"></i>
										<h3 class="nav-item__title">${ title }</h3>
										<button type="button" class="button-default nav-item__button">Dowiedz się więcej</button>
										<span class="number">0${ index }</span>
									</div>						
									`;
						return markup;
					},					
				},
				spaceBetween: 0,
				on: {
					init () {						
						let slider = this;
						let index = slider.activeIndex;

						back.addEventListener('click', (event) => {
							SWIPER.methods.goToFirstSlide(slider);
							anim.hideBackBar(event.currentTarget);
							anim.slideNavBack(navEl);
						}, false);

						anim.hideBackBar(back);
					},
					
					slideChange () {
						let index = this.activeIndex;
						let slide = this.slides[index];
						let title = slide.getAttribute('title')						
						backTitle.innerHTML = title;

						if(index !== 0) {
							anim.showBackBar(back);
							anim.slideNav(navEl);						
						}
					}					
				}
			});
		});		
	},

	copy (selector) {
		selector.forEach(function(el, index) {
			body.insertAdjacentHTML('beforeend', '<div class="swiper-pagination progress-y fade-in"></div>');
			el.insertAdjacentHTML('beforeend', '<button type="button" class="prev arrow arrow-prev u-hidden-sm">PPP</div>');			 
			el.insertAdjacentHTML('beforeend', '<button type="button" class="next arrow arrow-next u-hidden-sm">NNN</div>');			
			let prev = el.querySelector('.arrow-prev');
			let next = el.querySelector('.arrow-next');

	        let swiper = new Swiper(el, {
	            grabCursor: true,				
				// slidesPerView: 1,
				slidesPerView: 'auto',
				centeredSlides: true,
				navigation: {
					nextEl: next,
					prevEl: prev
				},
				pagination: {
					el: '.swiper-pagination',
					modifierClass: 'progress ',
					bulletActiveClass: 'active',
					clickable: true				
				},
				nested: true,
				direction: 'vertical',				
				spaceBetween: 40,
				mousewheel: true,
				breakpoints: {					
					768: {
						slidesPerView: 'auto',
						pagination: false,					
					},
				},
				on: {
					init () {
						this.params.watchOverflow = true;
						anim.arrowSlideIn(next);
						anim.arrowSlideOut(prev);						
					},

					reachBeginning () {
						anim.arrowSlideIn(next);
						anim.arrowSlideOut(prev);
					},

					reachEnd () {
						anim.arrowSlideIn(prev);
						anim.arrowSlideOut(next);						
					}					
				}			
			});
			
			(util.isMobile.any() || body.offsetWidth <= tablet) ? swiper.destroy() : '';			
		});
	},

	carousel (selector) {
		selector.forEach(function(el, index) {
	        let swiper = new Swiper(el, {
	            grabCursor: true,
				slidesPerView: 'auto',				
				mousewheel: true,
				spaceBetween: 0,
				nested: true,
	        });
		});
	},

	methods: {
		goToFirstSlide (slider) {
			slider.slideTo(0, 1000);
		}
	},

	init: function() {
		(pageSlider[0] !== null) ? SWIPER.page(pageSlider) : '';
		(copySliders[0] !== null) ? SWIPER.copy(copySliders) : '';	
		(carousel[0] !== null) ? SWIPER.carousel(carousel) : '';
	}
};

export default {
	init : SWIPER.init
};
