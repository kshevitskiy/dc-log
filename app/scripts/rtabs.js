import util from './utilities'

const menus = [...document.querySelectorAll('.rtabs')];
const tabs = document.querySelectorAll('.tabs-slider');

const MENU = {

    slider (selector) {
        selector.forEach(function(el, index) {
            const id = index;            
            const nav = MENU.navWrapper(id);
            const navID = `menu-${id}`;
            const hasNav = el.hasAttribute('data-nav');
            (hasNav && !util.isMobile.any()) ? el.parentNode.insertAdjacentHTML('beforeend', nav) : '';
            
            let swiper = new Swiper(el, {
                touchRatio: util.isMobile.any() ? 1 : 0,
                loop: util.isMobile.any() ? true : false,
                effect: 'fade',
				fadeEffect: {
					crossFade: true
				},
	            grabCursor: false,
                slidesPerView: 1,
                nested: true,    
				pagination: {
                    el: `#${navID}`,
                    clickable: true,
					bulletClass: 'tab-nav-item',
					bulletActiveClass: 'active',
                    modifierClass: 'tabs-nav ',
					renderBullet (index, className) {
						let slide = this.slides[index];
                        let iconURL = slide.getAttribute('data-icon');
                        let markup = `<div class="${ className }" data-wheelnav-navitemimg="${ iconURL }"></div>`;
                        return markup;
					},					
                },
                on: {
                    init () {
                        util.isMobile.any() ? '' : initMenu();

                        function initMenu() {
                            setTimeout(function() {
                                MENU.buildTabs(navID, swiper);
                            }, 1);
                        }
                    }
                }
            });
        });
    },

    navWrapper (id) {
        let nav = `
                    <div id="menu-${id}"
                        data-wheelnav
                        data-wheelnav-marker data-wheelnav-markerpath="PieLineMarker"
                        data-wheelnav-rotateoff
                        data-wheelnav-navangle="180"
                        data-wheelnav-cssmode 
                        data-wheelnav-init>
                    </div>
                `;
        return nav;
    },

    buildTabs (selector, slider) {
        const menu = new wheelnav(selector);
        const centerX = document.getElementById(selector).offsetWidth / 2;
        const centerY = document.getElementById(selector).offsetHeight / 2;
        menu.centerX = centerX;
        menu.centerY = centerY;
        menu.wheelRadius = menu.wheelRadius * 1;
        menu.slicePathFunction = slicePath().DonutSlice;
        menu.slicePathCustom = slicePath().DonutSliceCustomization();
        menu.slicePathCustom.maxRadiusPercent = 0.9;
        menu.slicePathCustom.minRadiusPercent = 0.68;
        menu.sliceSelectedPathCustom = slicePath().DonutSliceCustomization();
        menu.sliceSelectedPathCustom.maxRadiusPercent = 0.9;
        menu.sliceSelectedPathCustom.minRadiusPercent = 0.68;   
        menu.sliceAngle = menu.navAngle / menu.navItemCount * 1.8;
        menu.titleHeight = 24;
        menu.animatetime = 300;
        menu.animateeffect = 'linear';
        menu.createWheel();

        const tabs = menu.navItems;
        tabs.forEach(function(tab) {
            MENU.events(menu, tab, slider);            
        });

        MENU.methods.changeIcon(menu, menu.selectedNavItemIndex);
    },

    events (menu, tab, slider) {
        let tabNode = tab.navSlice.node;
        let titleNode = tab.navTitle.node;
        let tabIndex = tab.itemIndex;

        tabNode.addEventListener('click', () => { MENU.methods.changeSlide(menu, slider, tabIndex) }, false);
        tabNode.addEventListener('touchend', () => { MENU.methods.changeSlide(menu, slider, tabIndex) }, false);
        titleNode.addEventListener('click', () => { MENU.methods.changeSlide(menu, slider, tabIndex) }, false);        
        titleNode.addEventListener('touchend', () => { MENU.methods.changeSlide(menu, slider, tabIndex) }, false);
    },

    methods: {
        changeSlide (menu, slider, index) {
            slider.slideTo(index);
            menu.navigateWheel(index);
            MENU.methods.changeIcon(menu, index);
        },

        getIconSrc (menu, index) {
            let src = menu.navItems[index].title.split('imgsrc:').join('');
            return src;
        },

        changeIcon (menu, index) {
            let icon = MENU.methods.getIconSrc(menu, index);            
            let nav = document.getElementById(menu.holderId);
            nav.style.backgroundImage = `url('${icon}')`;
        }
    },

    init () { 
        MENU.slider(tabs);
    }
}

export default {
    init: MENU.init
}