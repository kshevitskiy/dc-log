import anime from 'animejs'
import util from './utilities'

const tablet = 922;
const mobile = 768;
const body = document.body;

const svgNS = "http://www.w3.org/2000/svg";
const svg = document.getElementById('page-loader-rect');
const logoLoader = document.createElement('div');
      logoLoader.setAttribute('class', 'logo-loader');
const offset = 80;
const fadeInUpNav = document.querySelectorAll('.nav-item.fade-in-up');
let fadeIn;

const curves = document.querySelectorAll('#waves path');
const curvePaths = [
    'M0,199V33S190,153.92,499.5,153.92C990.58,153.92,1065,0,1427.25,0,1712.37,0,1887,61.28,1920,79.14V196Z',
    'M0,19.8S278,204,552,204C1070,204,1064,.41,1478.38 .41,1780,0.41,1920,124,1920,124V285H1V19.8Z',
    'M0,48.38S291.69,211.11,565,211.11C945,211.11,1099,.5,1487,0.5,1779.39,0.5,1920,107,1920,107V352H0V48.38Z'
];

//** Get dynamic elements
let pageNavItems;
let progressBars;
Pace.on('done', function() {
    fadeIn = document.querySelectorAll('.fade-in');
    pageNavItems = document.querySelectorAll('#page-nav .nav-item');
    progressBars = document.querySelectorAll('.progress');
});

var ANIM = {

    page: {

        logo (selector) {
            let timeline = anime.timeline();
            
            timeline
                .add({
                    targets: selector,
                    opacity: 1,
                    scale: 1,
                    delay: 0
                })
                .add({
                    targets: selector,
                    opacity: 0,
                    scale: .7,
                    easing: 'easeOutExpo',
                    duration: 1000,
                }); 
        },

        svg (selector) {
            anime({
                targets: selector,
                strokeWidth: (util.isMobile.any() || body.offsetWidth <= tablet) ? 0 : 160,
                opacity: (util.isMobile.any() || body.offsetWidth <= tablet) ? 0 : 1,
                easing: 'easeOutExpo',
                delay: 1000
            });
        },

        waves (selector, path, delay = 0) {
            anime({
                targets: selector,
                d: path,
                delay: delay,
                elasticity: 200,
                duration: 3000,
                easing: 'easeOutExpo',
            });
        },

        loaders () {
            body.appendChild(logoLoader);
        },

        init () {
            ANIM.page.svg(svg);
            ANIM.page.logo(logoLoader);
                        
            for(let i = 0; i < curves.length; i++) {
                let delayTime = i * 50;
                let reverseIndex = curves.length - 1 - i;
                ANIM.page.waves(curves[reverseIndex], curvePaths[i], 1500 + delayTime);
            }
        }
    },

    slider: {

        hideBackBar (selector) {
            anime({
                targets: selector,
                translateX: (util.isMobile.any()) ? '0' : '100%',
                translateY: (util.isMobile.any()) ? '-100%' : '0',
                duration: 400,
                easing: 'easeOutQuad'
            });
        },

        showBackBar (selector) {
            anime({
                targets: selector,
                translateX: 0,
                translateY: 0,
                duration: 400,
                easing: 'easeOutQuad'
            });
        },

        slideNav (selector) {
            anime({
                targets: selector,
                translateX: (util.isMobile.any()) ? 0 : -80,
                translateY: (util.isMobile.any()) ? '-100%' : 0,
                duration: 500,
                easing: 'easeOutQuad'
            });	           
        },        
    
        slideNavBack (selector) {
            anime({
                targets: selector,
                translateX: 0,
                translateY: 0,
                duration: 400,
                easing: 'easeOutQuad'
            }); 
        },

        arrowSlideIn (selector) {
            anime({
                targets: selector,
                translateY: 0,
                opacity: 1,
                duration: 300,
                delay: 0,
                easing: 'easeOutQuad'
            });
        },

        arrowSlideOut (selector) {
            anime({
                targets: selector,
                translateY: '100%',
                opacity: 0,
                duration: 300,
                delay: 0,
                easing: 'easeOutQuad'
            });
        }
    },

    fadeIn: function(selector, hasDelay = false, delayDuration = 100) {
        let timeline = anime.timeline();

        timeline
            .add({
                targets: selector,
                opacity: 0,
                delay: 0
            })
            .add({
                targets: selector,
                opacity: 1,
                easing: 'easeOutExpo',
                duration: 1000,
                elasticity: 100,
                delay: hasDelay ? (selector, i, l) => { return i * delayDuration; } : 0
            });           
    },

    fadeInUp: function(selector, hasDelay = false, delayDuration = 100) {
        let timeline = anime.timeline();
        timeline
            .add({
                targets: selector,
                opacity: 0, 
                translateY: offset,
                delay: 0
            })
            .add({
                targets: selector,
                opacity: 1,
                translateY: 0,
                easing: 'easeOutExpo',
                duration: 1000,
                elasticity: 100,
                delay: hasDelay ? (selector, i, l) => { return i * delayDuration; } : 0
            });          
    }, 

    fadeInLeft: function(selector, hasDelay = false, delayDuration = 100) {
        let timeline = anime.timeline();
        timeline
            .add({
                targets: selector,
                opacity: 0, 
                translateX: offset,
                delay: 0
            })
            .add({
                targets: selector,
                opacity: 1,
                translateX: 0,
                easing: 'easeOutExpo',
                duration: 1000,
                elasticity: 100,
                delay: hasDelay ? (selector, i, l) => { return i * delayDuration; } : 0
            });          
    },     

	init () {
        ANIM.page.loaders();
        Pace.on('done', function() {
            ANIM.page.init();
            ANIM.fadeIn(fadeIn);
            ANIM.fadeInUp(fadeInUpNav, true);
            ANIM.fadeInLeft(progressBars);
            //** Page nav tabs animation
            (util.isMobile.any() || body.offsetWidth <= tablet) ? ANIM.fadeIn(pageNavItems, true) : ANIM.fadeInLeft(pageNavItems, true);
        });
	}
};

export default {
    init: ANIM.init,
    hideBackBar: ANIM.slider.hideBackBar,
    showBackBar: ANIM.slider.showBackBar,
    slideNav: ANIM.slider.slideNav,
    slideNavBack: ANIM.slider.slideNavBack,
    arrowSlideIn: ANIM.slider.arrowSlideIn,
    arrowSlideOut: ANIM.slider.arrowSlideOut
};