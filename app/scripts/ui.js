const doc = window.document
const docEl = doc.documentElement
const page = document.getElementById('page');
const pageMeta = document.getElementById('page-meta');
const pageNumber = document.getElementById('page-number');
const pageTitle = document.getElementById('page-title');
const pageNav = document.getElementById('page-main-nav');
const nav = document.getElementById('site-navigation');
const navButton = document.getElementById('menu');
const closeNav = document.getElementById('close-menu');
const navItem = document.querySelectorAll('.nav-grid .nav-item');
const fsToggle = document.getElementById('fsToggle');

const UI = {

	page: {
		title (selector) {			
			let title = selector.getAttribute('data-page-title');
			pageTitle.innerHTML = title;
		},

		icon (selector) {
			let icon = selector.getAttribute('data-page-icon');			
			// console.log(icon)
		},		

		number (selector) {
			let title = selector.getAttribute('data-page-title');
			let number = selector.getAttribute('data-page-number');
			if (number === '00') {
				pageMeta.style.display = 'none';
			}			
			pageNumber.innerHTML = number;
			pageNumber.setAttribute('title', title);
		},

		home (selector, nav) {
			let number = selector.getAttribute('data-page-number');
			let markup = `<a href="index.html" class="back-home fade-in"><span class="line">Wróć do strony głównej</span></a>`;
			if (number !== '00') {
				nav.insertAdjacentHTML('afterbegin', markup);
			}			
		},

		init () {
			UI.page.title(page);
			UI.page.icon(page);
			UI.page.number(page);
			UI.page.home(page, pageNav);
		}
	},

	navNumber: function(selector) {
		for(let i = 0; i < selector.length; i++) {
			let navNum = '0' + (i + 1);
			let numberWrapper = document.createElement('span');
				numberWrapper.setAttribute('class', 'number');
				numberWrapper.innerText = navNum;
			selector[i].appendChild(numberWrapper);
		}
	},

	navigation: {
		toggleNav () {
			nav.classList.toggle('active');
		},

		hideNav () {
			nav.classList.remove('active');
		},

		events () {
			navButton.addEventListener('click', UI.navigation.toggleNav, false);
			closeNav.addEventListener('click', UI.navigation.hideNav, false);
		},

		init () {
			UI.navigation.events();
		}
	},

	goFullScreen () {
		this.classList.remove('expanded');
		let requestFullScreen = docEl.requestFullscreen || docEl.mozRequestFullScreen || docEl.webkitRequestFullScreen || docEl.msRequestFullscreen
		let cancelFullScreen = doc.exitFullscreen || doc.mozCancelFullScreen || doc.webkitExitFullscreen || doc.msExitFullscreen
	
		if (!doc.fullscreenElement && !doc.mozFullScreenElement && !doc.webkitFullscreenElement && !doc.msFullscreenElement) {
		  requestFullScreen.call(docEl)
		  this.classList.add('expanded');
		} else {
		  cancelFullScreen.call(doc)
		}
	},

	events () {
		fsToggle.addEventListener('click', UI.goFullScreen, false);
	},

	init: function() {
		UI.page.init();
		UI.navNumber(navItem);
		UI.events();
		UI.navigation.init();		
	}
};

export default {
	init : UI.init
};
