//** Components
import swiper from './swiper'
import animation from './animations'
import ui from './ui'
import rtabs from './rtabs'

window.onload = function() {
	swiper.init();
	animation.init();
	ui.init();
	rtabs.init();
};

